/* eslint-disable react/no-array-index-key */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ccn from 'ccn';
import bem from 'bem';
import isEmpty from 'lodash/isEmpty';
import onClickOutside from 'react-onclickoutside';
import Button from 'components/button';
import Input from '../part-input';
import { formErrorDecorator } from '../part-error';
import styles from './input-suggestions.scss';

@formErrorDecorator
@onClickOutside
@ccn
@bem
export default class UniFormInputSuggestions extends Component {
  static propTypes = {
    options: PropTypes.array, // eslint-disable-line
    onChange: PropTypes.func,
    onSelect: PropTypes.func,
  };

  constructor(props) {
    super(props);

    this.state = { isOptionsVisible: false };

    this.onChange = this.onChange.bind(this);
  }

  onChange(...args) {
    const { onChange } = this.props;
    if (onChange) onChange(...args);
    this.setState({ isOptionsVisible: true });
  }

  onSelect(option) {
    const { onSelect } = this.props;
    if (onSelect) onSelect(option);
    this.setState({ isOptionsVisible: false });
  }

  handleClickOutside() {
    this.setState({ isOptionsVisible: false });
  }

  render() {
    const { options, ...rest } = this.props;
    const { isOptionsVisible } = this.state;
    const toShowOptions = !isEmpty(options) && isOptionsVisible;

    return (
      <div
        data-block="uniFormInputSuggestions"
        data-bemstyles={styles}>
        <Input {...rest} onChange={this.onChange} />
        {toShowOptions && <div data-elem="options">
          {options.map((option, i) => {
            const { value: title = '' } = option;
            return (
              <Button
                data-elem="optionItem"
                data-mods={{ first: i === 0, last: i === options.length - 1 }}
                key={i}
                onClick={() => this.onSelect(option)}
              >
                {title}
              </Button>
            );
          })}
        </div>
        }
      </div>
    );
  }
}
