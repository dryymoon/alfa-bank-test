/* eslint-disable function-paren-newline, no-unused-vars, max-len */
// import Default from './page-default';
const pageLogin = () => import(/* webpackChunkName: "page-login" */'./page-login');

const routes = [
  {
    path: '/',
    onEnter: (_, replace) => replace('/login')
  },
  {
    name: 'login',
    path: '/login',
    getComponent(l, cb) {
      pageLogin().then(module => cb(null, module.default));
    }
  }
];

export default routes;

/*
{
          name: 'lazy',
          path: '/lazy/',
          getComponent(l, cb) {
            pageLazy().then(module => cb(null, module.default));
          }
 */
