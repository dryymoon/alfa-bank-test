import { H1, H2 } from './part-h/h';

export Radio from './part-radio';
export Input from './part-input';
export Textarea from './part-textarea';
export InputColor from './part-input-color';
export InputSuggestions from './part-input-suggestions';
export InputNumber from './part-input-number';
export Error from './part-error-cont';
export CheckBox from './part-checkbox';
export Select from './part-select';
export { H1, H2 };

export default from './part-form';
