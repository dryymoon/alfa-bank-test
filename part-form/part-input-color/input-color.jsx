import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { t } from 'c-3po';
import { Validator as colorValidator } from 'collit';
import Input from '../part-input';

export default class UniFormInputColor extends Component {
  static propTypes = {
    value: PropTypes.string,
    onChange: PropTypes.func,
  };

  constructor(props) {
    super(props);
    const { value = '' } = props;
    this.onChange = this.onChange.bind(this);
    this.state = {
      value,
      isValid: colorValidator.isColor(value)
    };
  }

  componentWillReceiveProps(nextProps) {
    const isPropsValueChanged = this.props.value !== nextProps.value;
    const isStateValueDifferentWithProps = this.state.value !== nextProps.value;
    if (isPropsValueChanged && isStateValueDifferentWithProps) {
      this.setState({
        value: nextProps.value,
        isValid: colorValidator.isColor(nextProps.value)
      });
    }
  }

  onChange(target) {
    const { onChange } = this.props;
    const { value = '' } = target;

    const isValidColor = colorValidator.isColor(value);

    this.setState({ value, isValid: isValidColor }, () => {
      if (isValidColor && onChange) onChange(target);
    });
  }

  render() {
    const { value = '', isValid } = this.state;
    let errorStr = '';
    if (!isValid) errorStr = t`Невалидный цвет`;
    return (
      <Input {...this.props} {...{ value, errorStr, onChange: this.onChange }} />
    );
  }
}
