import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ccn from 'ccn';
import bem from 'bem';
import { connect } from 'react-redux';
import assign from 'lodash/assign';
import values from 'lodash/values';
import uniqueId from 'uniqueId';
import uuid from 'uuid/v4';

import * as redux from './redux';
import styles from './part-form.scss';

@connect(null, { ...redux })
@ccn
@bem
@uniqueId
export default class UniForm extends Component {
  static propTypes = {
    children: PropTypes.node,
    isNew: PropTypes.bool,
    init: PropTypes.func,
    name: PropTypes.string,
    destroy: PropTypes.func,
    setRef: PropTypes.func,
    onSubmit: PropTypes.func,
  };

  static childContextTypes = {
    formId: PropTypes.string,
    formIsNew: PropTypes.bool,
    formRegisterErrorComponentRef: PropTypes.func,
  };

  constructor(props, ctx) {
    super(props, ctx);
    this.registeredInteractiveComponents = {};
    this.formRegisterErrorComponentRef = this.formRegisterErrorComponentRef.bind(this);
    this.formUnRegisterErrorComponentRef = this.formUnRegisterErrorComponentRef.bind(this);
    this.preventSubmit = this.preventSubmit.bind(this);
    this.submit = this.submit.bind(this);
  }

  getChildContext() {
    const { formRegisterErrorComponentRef, formId } = this;
    const { isNew } = this.props;
    return {
      formId,
      formIsNew: isNew,
      formRegisterErrorComponentRef
    };
  }

  componentWillMount() {
    const formId = `form_${this.uid}`;
    assign(this, { formId });
    this.formId = `form_${this.uid}`;
    // TODO Implement Set Old Form
    const { isNew, setRef } = this.props;
    if (setRef) setRef(this);
    this.props.init({ formId, isNew });
  }

  componentWillUnmount() {
    const { formId } = this;
    this.props.destroy({ formId });
  }

  resetInteraction() {
    values(this.registeredInteractiveComponents)
      .filter(it => it)
      .forEach(({ resetInteraction } = {}) => resetInteraction && resetInteraction());
  }

  formRegisterErrorComponentRef(cmpRef) {
    const uid = uuid();
    this.registeredInteractiveComponents[uid] = cmpRef;
    return () => this.formUnRegisterErrorComponentRef(uid);
  }

  formUnRegisterErrorComponentRef(cmpUid) {
    this.registeredInteractiveComponents[cmpUid] = null;
    delete this.registeredInteractiveComponents[cmpUid];
  }

  submit() {
    this.dom.submit();
  }

  preventSubmit(e) {
    const { onSubmit } = this.props;
    if (onSubmit) return onSubmit(e);
    e.preventDefault();
    return false;
  }

  render() {
    const { children, isNew, name } = this.props;
    return (
      <form
        name={name}
        onSubmit={this.preventSubmit}
        data-block="uniForm"
        data-bemstyles={styles}
        data-mods={{ isNew }}
        children={children}
        ref={el => this.dom = el}
      />
    );
  }
}
