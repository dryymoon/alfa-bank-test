/* eslint-disable react/no-array-index-key, react/no-multi-comp */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ccn from 'ccn';
import bem from 'bem';
import { t } from 'c-3po';
import { connect } from 'react-redux';
import uniqueId from 'uniqueId';
import isEmpty from 'lodash/isEmpty';
import isString from 'lodash/isString';
import debounce from 'lodash/debounce';
import get from 'lodash/get';
import uniq from 'lodash/uniq';
import pick from 'lodash/pick';
import omit from 'lodash/omit';
import keys from 'lodash/keys';

import * as redux from '../redux';
import styles from './part-error.scss';

const errPropTypes = {
  errorStr: PropTypes.string, // eslint-disable-line react/no-unused-prop-types
  // eslint-disable-next-line react/no-unused-prop-types
  errorArr: PropTypes.arrayOf(PropTypes.string),
  errorObj: PropTypes.object, // eslint-disable-line
  errorContDynamic: PropTypes.bool,
  errorContHidden: PropTypes.bool,
  errorDummy: PropTypes.node
};

export const shape = {
  ...errPropTypes,
  formId: PropTypes.string,
  formIsNew: PropTypes.bool,
  isValid: PropTypes.bool,
  hasError: PropTypes.bool,
  onInteract: PropTypes.func,
  errorProps: PropTypes.object // eslint-disable-line
};

/* @connect(({ [redux.key]: formsStates = {} }, { formId } = {}) => {
 const { [formId]: { isNew } = {} } = formsStates;
 return { formIsNew: isNew };
 }) */
@ccn
@bem
export default class UniFormError extends Component {
  static propTypes = {
    ...errPropTypes,
    // interacted: PropTypes.bool,
    // formIsNew: PropTypes.bool,
    isValid: PropTypes.bool,
    hasError: PropTypes.bool,
    // errors: PropTypes.array, //eslint-disable-line
    enabled: PropTypes.bool,
  };

  render() {
    const {
      errorContDynamic, errorContHidden, // errorDummy,
      isValid, hasError, enabled,
      errorStr, errorArr, errorObj
      // formIsNew, interacted
    } = this.props;
    if (!enabled) return null;

    let errors = [];

    if (errorStr) errors.push(errorStr);
    if (errorArr && errorArr.length > 0) errors = [...errors, errorArr];
    if (errorObj && !isEmpty(errorObj)) errors = [...errors, ...Object.keys(errorObj)];
    let showContainer = true;
    if (errorContDynamic && isValid) showContainer = false;
    if (errorContHidden) showContainer = false;

    const showDummy = !errorContDynamic && errors && errors.length === 0;
    let translatedErrors = errors.map((it) => {
      if (it === 'hasDuplicatedItems') return t`Есть дублирующиеся поля`;
      if (it === 'duplicatedItem') return t`Дублирующееся поле`;
      if (it === 'requiredItemIsEmpty') return t`Заполните обязательное поле`;
      if (it === 'requiredMinLength') return t`Это поле должно быть длинее и заполнено`;
      if (it === 'requiredMinItems') return t`Выберите большее количество элементов`;
      if (it === '$maxItems' && get(errorObj, [it, 'params', 'limit'])) {
        const limit = get(errorObj, [it, 'params', 'limit']);
        return t`Первышено максимальное количество. Максимум ${limit} шт.`;
      }
      if (it === '$maxLength' && get(errorObj, [it, 'params', 'limit'])) {
        const limit = get(errorObj, [it, 'params', 'limit']);
        return t`Превышена максимальная длина в ${limit} символов`;
      }

      if (it === '$maximum' && get(errorObj, [it, 'params', 'limit'])) {
        const limit = get(errorObj, [it, 'params', 'limit']);
        return t`Первышено максимальное значение. Максимум ${limit}`;
      }

      if (it[0] === '$' && !get(errorObj, [it, 'value'])) {
        return t`Заполните обязательное поле`;
      }

      if (it === '$pattern') {
        const currPattern = get(errorObj, [it, 'params', 'pattern']);
        const onlyDigitsPattern = '^[\\d]+$';

        if (currPattern === onlyDigitsPattern) {
          return t`Поле принимает только числовые значения`;
        }
        return t`Поле не соответствует шаблону`;
      }

      if (isString(it)) return it;
      return t`Неизвестная ошибка валидации`;
    });

    translatedErrors = uniq(translatedErrors);

    return (
      <div
        data-block="uniFormError"
        data-bemstyles={styles}
        data-mods={{ isValid, hasError }}>
        {showContainer && (
          <div data-elem="errorContainer">
            {showDummy && <div data-elem="error" data-mods="dummy">&nbsp;</div>}
            {!showDummy && translatedErrors.map((err, ind) =>
              <div data-elem="error" key={ind} data-mods="real">{err}</div>)}
          </div>)}
      </div>
    );
    // dummy надо чтоб форма не прыгала... когда ошибок нету

    /* errorDummy && showDummy && <div data-elem="error"
     data-mods="dummyComponent">{errorDummy}</div> */
  }
}

const errPropTypesKeys = keys(errPropTypes);

/* eslint-disable react/prop-types */
export function formErrorDecorator(BaseComponent) {
  @connect(null, { ...redux }) // null, { withRef: true }
  @uniqueId
  class FormComponentWithErrors extends Component {
    static propTypes = {
      ...errPropTypes,
      /* redux */
      registerError: PropTypes.func,
      dropError: PropTypes.func
    };

    static contextTypes = {
      formId: PropTypes.string,
      formIsNew: PropTypes.bool,
      formErrorHideChildErrors: PropTypes.bool,
      formRegisterErrorComponentRef: PropTypes.func,
    };

    /* static getInstance(instance) {
     if (!(instance && instance.getWrappedInstance)) {
     if (__DEVELOPMENT__) throw new Error('Form getInstance called without instance aggument.');
     return false;
     }
     return instance.getWrappedInstance();
     } */

    constructor(props, ctx) {
      super(props, ctx);
      this.state = {};
      this.onInteract = this.onInteract.bind(this);
      // подпорка, потому что некоторые валидаторы асинхронные и
      // срабатывают через 500мс после действий
      this.firstInteractionFinished = debounce(this.firstInteractionFinished.bind(this), 600);
      this.resetInteraction = this.resetInteraction.bind(this);
    }

    componentWillMount() {
      this.errorContainerId = `errorCnt_${this.uid}`;
      this.isErrorRegistered = false;

      const { errorContainerId } = this;
      const { formId, formRegisterErrorComponentRef } = this.context;
      const { registerError } = this.props;
      const { hasError, errors } = this.calcErrors();
      if (hasError && formId) {
        registerError({ formId, errorContainerId, errorData: errors });
        this.isErrorRegistered = true;
      }
      if (formRegisterErrorComponentRef) {
        this.unregisterFn = formRegisterErrorComponentRef(this);
      }
    }

    componentDidMount() {
      this.componentMounted = true;
    }

    componentWillReceiveProps(nextProps, nextCtx) {
      const { errorContainerId, isErrorRegistered } = this;
      const { formId } = nextCtx;
      const { registerError, dropError } = nextProps;
      const { isValid, hasError, errors } = this.calcErrors(nextProps);
      if (hasError && !isErrorRegistered && formId) {
        registerError({ formId, errorContainerId, errorData: errors });
        this.isErrorRegistered = true;
      }
      if (isValid && isErrorRegistered && formId) {
        dropError({ formId, errorContainerId });
        this.isErrorRegistered = false;
      }
    }

    componentWillUnmount() {
      this.componentMounted = false;
      if (this.unregisterFn) this.unregisterFn();
    }

    onInteract() {
      const { firstInteracted } = this.state;
      if (!firstInteracted) this.firstInteractionFinished();
    }

    resetInteraction() {
      const { firstInteracted } = this.state;
      this.firstInteractionFinished.cancel();
      if (firstInteracted) this.setState({ firstInteracted: false });
    }

    firstInteractionFinished() {
      if (this.componentMounted) this.setState({ firstInteracted: true });
    }

    calcErrors(props = this.props) {
      const { errorStr, errorArr, errorObj } = props;
      // let errors = [];

      // if (errorStr) errors.push(errorStr);
      // if (errorArr && errorArr.length > 0) errors = [...errors, errorArr];
      // if (errorObj && !isEmpty(errorObj)) errors = [...errors, ...Object.keys(errorObj)];

      let isValid = true;
      if (errorStr) isValid = false;
      if (errorArr && errorArr.length > 0) isValid = false;
      if (errorObj && !isEmpty(errorObj)) isValid = false;

      const hasError = !isValid;

      return { isValid, hasError };
    }

    render() {
      const { formIsNew, formErrorHideChildErrors } = this.context;

      const { firstInteracted } = this.state;

      const errorConfigureProps = pick(this.props, errPropTypesKeys);

      if (isEmpty(errorConfigureProps)) return <BaseComponent {...this.props} />;

      const elemProps = omit(this.props, errPropTypesKeys);

      // TODO сделать лучше
      let { isValid, hasError } = this.calcErrors();
      let { errorStr, errorArr, errorObj } = this.props;
      if (formIsNew && !firstInteracted) {
        isValid = true;
        hasError = false;
        errorStr = undefined;
        errorArr = undefined;
        errorObj = undefined;
      }

      const errorProps = {
        ...errorConfigureProps,
        isValid,
        hasError,
        errorStr,
        errorArr,
        errorObj,
        enabled: true
      };

      if (formErrorHideChildErrors) errorProps.errorContHidden = true;

      return (<BaseComponent
        {...elemProps}
        {...{ isValid, hasError, errorProps, onInteract: this.onInteract }} />);
    }
  }

  return FormComponentWithErrors;
}
