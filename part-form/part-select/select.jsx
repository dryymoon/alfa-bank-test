/* eslint-disable no-unused-vars */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ccn from 'ccn';
import bem from 'bem';
import { t } from 'c-3po';
import pick from 'lodash/pick';
import keys from 'lodash/keys';
import assign from 'lodash/assign';
import isEmpty from 'lodash/isEmpty';

import pureRender from 'pure-render-decorator';
import DropDown, { close, shape as DDShape } from 'components/drop-down';
import FlexBox from 'components/flexbox';
import Button from 'components/button';
import InfinityWrapper, { InfinityLoader, InfinityWrapperShape } from 'components/infinity-scroll';
import ArrowIcon from './arrow.svg';

import Error, { formErrorDecorator, shape } from '../part-error';
import styles from './select.scss';

@formErrorDecorator
@ccn
@bem
@pureRender
export default class UniFormSelect extends Component {
  static propTypes = {
    label: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    options: PropTypes.array, // eslint-disable-line
    onSelect: PropTypes.func,
    textKey: PropTypes.string,
    disabled: PropTypes.bool,
    ...shape
  };

  constructor(props) {
    super(props);
    this.onCloseDD = this.onCloseDD.bind(this);
  }

  onSelect(...args) {
    const { onSelect } = this.props;
    let needClose = true;
    if (onSelect) needClose = onSelect(...args);
    if (needClose !== false && this.ddRef) close(this.ddRef);
  }

  onCloseDD() {
    const { onInteract } = this.props;
    if (onInteract) onInteract();
  }

  renderOptions() {
    const { options = [], textKey = 'text', 'data-test': dataTest } = this.props;
    return options.map((option, index) => {
      const { [textKey]: text, key, $ref } = option || {};
      if (!text) return null;
      const dataTestAttr = dataTest ? { 'data-test': `${dataTest}_itemSelectBtn_${key || index}` } : {};

      const extraPropsItem = {};
      if ($ref) assign(extraPropsItem, { ref: ref => $ref(ref) });
      return (
        <Button
          {...dataTestAttr}
          {...extraPropsItem}
          data-elem="contentItem"
          data-mods={{
            first: index === 0,
            last: index === options.length - 1
          }}
          key={key || text || index}
          onClick={() => this.onSelect(option, index, options)}>
          {text}
        </Button>
      );
    });
  }

  render() {
    const {
      options = [], label = t`Выберите вариант`,
      textKey = 'text', isValid, hasError, errorProps,
      'data-test': dataTest, disabled,
      setRefContainerBody
    } = this.props;

    const DDProps = pick(this.props, keys(DDShape));
    const infinityProps = pick(this.props, keys(InfinityWrapperShape));
    const hasInfinity = !isEmpty(infinityProps);

    const extraPropsContainerBody = {};
    if (setRefContainerBody) {
      assign(
        extraPropsContainerBody,
        { ref: node => setRefContainerBody(node) }
      );
    }

    return (
      <div
        data-block="uniFormSelect"
        data-bemstyles={styles}
        data-mods={{
          isValid,
          hasError,
          disabled
        }}>
        <DropDown
          {...DDProps}
          data-elem="dropDown"
          onClose={this.onCloseDD}
          ref={ref => this.ddRef = ref}>
          <Button data-elem="button" disabled={disabled}>
            <FlexBox row spaceBetweenContent data-elem="buttonInner">
              <span data-elem="buttonLabel">
                {label}
              </span>
              <span data-elem="buttonArrow">
                <ArrowIcon />
              </span>
            </FlexBox>
          </Button>
          <div data-elem="content">
            {hasInfinity &&
            <InfinityWrapper {...infinityProps}>
              <div
                {...extraPropsContainerBody}
                data-elem="contentBody"
              >
                {this.renderOptions()}
                <div key="loader">
                  <InfinityLoader gray />
                </div>
              </div>
            </InfinityWrapper>}
            {!hasInfinity &&
            <div
              {...extraPropsContainerBody}
              data-elem="contentBody"
            >
              {this.renderOptions()}
            </div>}
          </div>
        </DropDown>
        <Error {...errorProps} />
      </div>
    );
  }
}
