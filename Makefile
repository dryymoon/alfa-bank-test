build:
	docker build --tag alfa-bank-test:latest ./

run:
	docker run --rm --publish 8080:80 --name alfa-bank-test alfa-bank-test:latest

kill:
	docker rm --force alfa-bank-test
