/**
 * Created by dryymoon on 19.07.17.
 */

import ReducersRegistry from 'core/helpers/redux/reducers-registry';
import omit from 'lodash/omit';
import isEmpty from 'lodash/isEmpty';

export const key = 'form';

const INIT = `${key}/INIT`;
const DESTROY = `${key}/DESTROY`;
const REGISTER_ERROR = `${key}/REGISTER_ERROR`;
const DROP_ERROR = `${key}/DROP_ERROR`;
const SET_OLD_FORM = `${key}/SET_OLD_FORM`;

const REGISTER_FORM_COMPONENT = `${key}/REGISTER_FORM_COMPONENT`;
const UNREGISTER_FORM_COMPONENT = `${key}/UNREGISTER_FORM_COMPONENT`;

export default function reducer(state = {}, action = {}) {
  const { formId, type, ...restAction } = action;

  switch (type) {
    case INIT:
      return { ...state, [formId]: { ...restAction } };

    case DESTROY:
      return omit(state, [formId]);

    case REGISTER_ERROR: {
      let { [formId]: formState } = state;
      let { errors } = formState;

      errors = { ...errors, [action.errorContainerId]: action.errorData };
      formState = { ...formState, hasError: true, isValid: false, errors };

      return { ...state, [formId]: formState };
    }

    case DROP_ERROR: {
      let { [formId]: formState } = state;
      let { errors } = formState;

      errors = omit(errors, [action.errorContainerId]);
      const isValid = !!isEmpty(errors);
      const hasError = !isValid;

      formState = { ...formState, hasError, isValid, errors };

      return { ...state, [formId]: formState };
    }

    case SET_OLD_FORM: {
      const { [formId]: formState } = state;
      return { ...state, [formId]: { ...formState, isNew: false } };
    }

    case REGISTER_FORM_COMPONENT: {
      return state;
    }

    case UNREGISTER_FORM_COMPONENT: {
      return state;
    }

    default:
      return state;
  }
}

export function init({ formId, isNew }) {
  return { type: INIT, formId, isNew };
}

export function destroy({ formId }) {
  return { type: DESTROY, formId };
}

export function registerError({ formId, errorContainerId, errorData }) {
  return { type: REGISTER_ERROR, formId, errorContainerId, errorData };
}

export function dropError({ formId, errorContainerId }) {
  return { type: DROP_ERROR, formId, errorContainerId };
}

export function registerFormComponent({ formId, errorContainerId, errorData }) {
  return { type: REGISTER_FORM_COMPONENT, formId, errorContainerId, errorData };
}

export function unRegisterFormComponent({ formId, errorContainerId, errorData }) {
  return { type: UNREGISTER_FORM_COMPONENT, formId, errorContainerId, errorData };
}


export function setOldForm({ formId }) {
  return { type: SET_OLD_FORM, formId };
}

ReducersRegistry.register({ [key]: reducer });
