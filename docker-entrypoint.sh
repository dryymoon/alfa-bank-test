#!/bin/bash

# : "${API_URL:?The environment variable 'API_URL' must be set and non-empty}"

cat > ./runtime.js << EOF
window.__API_URL__ = '$API_URL';
EOF

exec "$@"
