#####For run test env only need to run next commands
```
git clone git@gitlab.com:dryymoon/alfa-bank-test.git
cd ./alfa-bank-test
make build
make run
```
Goto http://localhost:8080

#####For free resources
```
make kill
```

#####How to start develop:

needed packages:
* node v10+
* npm

```
git clone git@gitlab.com:dryymoon/dryymoon-react-boilerplate.git
npm install
cd ./src-sites/ && md ./$YOUR_PROJECT_NAME && cd ./$YOUR_PROJECT_NAME
git clone your your project
npm install
cd ../../
npm run dev-cli
```
* open in browser http://localhost:3000
* enter needed port for your project (3001)
* check checkbox for run build
* open in browser http://localhost:{port of your project}
* enjoy



