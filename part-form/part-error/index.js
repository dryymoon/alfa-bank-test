import Error, { shape, formErrorDecorator } from './part-error';

export default Error;

export { shape, formErrorDecorator };
