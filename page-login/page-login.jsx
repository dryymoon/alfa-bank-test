/* eslint-disable import/no-webpack-loader-syntax, import/first, jsx-a11y/alt-text */
import React from 'react';
import { linkDecorator } from 'components/link';
import Helmet from 'react-helmet';
import Button from 'components/button';
import Loader from 'components/loader';
import ccn from 'ccn';
import bem from 'bem';
import isEmpty from 'lodash/isEmpty';
// import NotificationsSystem from 'reapop';
import './page-login.scss';
import backgroundImgPlaceholder
  from '!!base64-inline-loader!jimp-loader?quality=25&resize[width]=500!./assets/lake-5534341_1920.jpg';
import backgroundImg from './assets/lake-5534341_1920.jpg';
import Form, { Input } from '../part-form';
import Tooltip from '../../../src-core/components/tooltip';

@ccn
@bem
@linkDecorator
export default class PageLogin extends React.Component {
  state = {};

  bgImgOnLoad = () => {
    this.setState({ bgImgLoaded: true });
  }

  tryLogin = () => {
    this.setState({ $loginReqRunning: true });
    setTimeout(() => this.setState({ $loginReqRunning: false }), 3000);
  }

  render() {
    const { bgImgLoaded, loginBtnInteracted, login, password, $loginReqRunning } = this.state;

    // TODO Refactor to throttle validatin, not in realtime
    const errors = {};
    if (!login) errors.login = 'No login filled';
    if (!password) errors.password = 'No password filled';

    return (
      <div data-block="pageLogin">
        <Helmet>
          <title>Login</title>
        </Helmet>
        <div data-elem="backgroundImgContainer">
          <img data-elem="backgroundImgPlaceholder" src={backgroundImgPlaceholder} alt="bg img" />
          <img
            data-elem="backgroundImg"
            data-mods={{ loaded: bgImgLoaded }}
            src={backgroundImg}
            alt="bg img"
            onLoad={this.bgImgOnLoad} />
        </div>
        <Form
          isNew={!loginBtnInteracted}
          data-elem="contentContainer"
        >
          <div data-elem="contentContainerBG">
            <div data-elem="contentContainerBGLayer0" />
            <div data-elem="contentContainerBGLayer1" />
            <div data-elem="contentContainerBGLayer2" />
            <div data-elem="contentContainerBGLayer3" />
          </div>
          <div data-elem="title">
            <span>Group Company</span>
          </div>
          <div data-elem="loginContainer">
            <Input
              value={login || ''}
              onChange={({ value }) => this.setState({ login: value })}
              placeholder="Username or email"
              errorStr={errors.login}
            />
          </div>
          <div data-elem="passwordContainer">
            <Input
              value={password || ''}
              onChange={({ value }) => this.setState({ password: value })}
              placeholder="Password"
              errorStr={errors.password}
            />
          </div>

          <Tooltip title={
            (!isEmpty(errors) && 'Form has errors')
            ||
            ($loginReqRunning && 'Request running')
          }>
            <Button
              onClick={this.tryLogin}
              data-elem="loginBtn"
              data-mods={{ disabled: false }}
              onMouseEnter={() => this.setState({ loginBtnInteracted: true })}
              disabled={loginBtnInteracted && !isEmpty(errors)}
            >
              <Loader gray active={$loginReqRunning}>
                <span>Login</span>
              </Loader>
            </Button>
          </Tooltip>
        </Form>
      </div>
    );
  }
}
