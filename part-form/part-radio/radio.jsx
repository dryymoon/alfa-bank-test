/* eslint-disable jsx-a11y/label-has-for */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ccn from 'ccn';
import bem from 'bem';
import has from 'lodash/has';
import assign from 'lodash/assign';
import uniqueId from 'uniqueId';
import Button from 'components/button';
import FlexBox from 'components/flexbox';
import styles from './radio.scss';

@ccn
@bem
@uniqueId
export default class UniFormRadio extends Component {
  static propTypes = {
    name: PropTypes.string,
    label: PropTypes.string,
    value: PropTypes.string,
    'data-test': PropTypes.string,
    checked: PropTypes.bool,
    onChange: PropTypes.func,
    disabled: PropTypes.bool,
  };

  constructor() {
    super();
    this.onClick = this.onClick.bind(this);
  }

  onClick(...args) {
    const { onChange } = this.props;
    if (onChange) return onChange(...args);
  }

  render() {
    const { label, checked, disabled, 'data-test': dataTest } = this.props;
    // eslint-disable-next-line
    if(has(this.props, 'defaultChecked')) console.warn('Default checke deprecated');
    const extend = {};
    if (dataTest) assign(extend, { 'data-test': dataTest });
    if (dataTest && !!checked) assign(extend, { 'data-test-value-checked': true });
    return (
      <Button
        data-block="uniRadioBox"
        data-mods={{ checked, disabled }}
        data-bemstyles={styles}
        onClick={this.onClick}
        disabled={disabled}
        {...extend}>
        <FlexBox row data-elem="inner">
          <div data-elem="box">
            <div data-elem="boxCheck" />
          </div>
          {label && <div data-elem="label">{label}</div>}
        </FlexBox>
      </Button>
    );
  }
}
