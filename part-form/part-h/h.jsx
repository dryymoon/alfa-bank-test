/**
 * Created by dryymoon on 28.07.17.
 */
/* eslint-disable react/prop-types, react/no-multi-comp */
import React, { Component } from 'react';
import bem from 'bem';
import ccn from 'ccn';

import styles from './h.scss';

@ccn
@bem
export class H1 extends Component {
  render() {
    const { children } = this.props;
    return (
      <div
        data-block="uniFormH1"
        data-bemstyles={styles}
        children={children}
      />
    );
  }
}

@ccn
@bem
export class H2 extends Component {
  render() {
    const { children } = this.props;
    return (
      <div
        data-block="uniFormH2"
        data-bemstyles={styles}
        children={children}
      />
    );
  }
}
