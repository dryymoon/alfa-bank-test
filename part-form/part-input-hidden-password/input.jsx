/* eslint-disable jsx-a11y/no-static-element-interactions */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ccn from 'ccn';
import bem from 'bem';
import assign from 'lodash/assign';

import Error, { formErrorDecorator, shape } from '../part-error';
import * as styles from './input.scss';

@formErrorDecorator
@ccn
@bem
export default class UniFormInput extends Component {
  static propTypes = {
    id: PropTypes.string,
    placeholder: PropTypes.string,
    stringNumbersOnly: PropTypes.bool,
    positiveNumbersOnly: PropTypes.bool,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    defaultValue: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    type: PropTypes.string,
    disabled: PropTypes.bool,
    onChange: PropTypes.func,
    onWrapperClick: PropTypes.func,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    onKeyUp: PropTypes.func,
    onKeyDown: PropTypes.func,
    'data-test': PropTypes.string,
    setRef: PropTypes.func,
    ...shape
  };

  onBlur(...args) {
    const { onBlur, onInteract } = this.props;
    if (onBlur) onBlur(...args);
    if (onInteract) onInteract();
  }

  onChange({ target, preventDefault }) {
    const { initialValue } = this;
    const { onChange, positiveNumbersOnly, stringNumbersOnly } = this.props;
    let notNeedPrevent = true;

    let { value = '' } = target;

    if (stringNumbersOnly) value = value.replace(/[^\d]/g, '');
    if (positiveNumbersOnly) value = Number(value.replace(/[^\d]/g, ''));

    if (initialValue === value) return;

    if (onChange) notNeedPrevent = onChange({ ...target, value });

    if (notNeedPrevent === false) preventDefault();
  }

  render() {
    const {
      id, value, placeholder,
      type = 'text', defaultValue, isValid, hasError,
      errorProps, 'data-test': dataTest, disabled,
      setRef, onFocus, onKeyUp, onKeyDown, wrapperProps, inputProps
    } = this.props;
    const extend = { ...inputProps, id, type, placeholder, disabled };
    if (dataTest) assign(extend, { 'data-test': dataTest });
    if (value !== undefined) assign(extend, { value: value || '' });
    if (defaultValue !== undefined) assign(extend, { defaultValue: defaultValue || '' });
    if (setRef) assign(extend, { ref: node => setRef(node) });

    this.initialValue = value;

    return (
      <div
        {...wrapperProps}
        data-block="uniFormInputHiddenPassword"
        data-mods={{ isValid, hasError, disabled }}
        data-bemstyles={styles}
      >
        <input
          {...extend}
          data-elem="input"
          onFocus={(...args) => onFocus && onFocus(...args)}
          onKeyUp={(...args) => onKeyUp && onKeyUp(...args)}
          onKeyDown={(...args) => onKeyDown && onKeyDown(...args)}
          onBlur={(...args) => this.onBlur(...args)}
          onChange={(...args) => this.onChange(...args)} />
        <Error {...errorProps} />
      </div>
    );
  }
}
