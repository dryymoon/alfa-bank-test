import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ccn from 'ccn';
import bem from 'bem';

import Error, { formErrorDecorator, shape } from '../part-error';
import * as styles from './error-cont.scss';

@formErrorDecorator
@ccn
@bem
export default class UniFormErrorCont extends Component {
  static propTypes = {
    children: PropTypes.node,
    hideChildErrorsOnInvalid: PropTypes.bool,
    'data-test': PropTypes.string,
    ...shape
  };

  static childContextTypes = { formErrorHideChildErrors: PropTypes.bool, };

  getChildContext() {
    const { hasError, hideChildErrorsOnInvalid } = this.props;
    let formErrorHideChildErrors = false;
    if (hasError && hideChildErrorsOnInvalid) formErrorHideChildErrors = true;
    return { formErrorHideChildErrors };
  }

  render() {
    const { children, isValid, hasError, errorProps, 'data-test': dataTest } = this.props;
    const dataTestAttr = dataTest ? { 'data-test': dataTest } : {};

    return (
      <div
        data-block="uniFormErrorCont"
        data-mods={{ isValid, hasError }}
        data-bemstyles={styles}>
        <div data-elem="group" children={children} {...dataTestAttr} />
        <Error data-elem="error" {...errorProps} />
      </div>
    );
  }
}

