/* eslint-disable jsx-a11y/label-has-for, no-unused-vars */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ccn from 'ccn';
import bem from 'bem';
import Button from 'components/button';
import FlexBox from 'components/flexbox';
import * as styles from './checkbox.scss';

@ccn
@bem
export default class UniCheckbox extends Component {
  static propTypes = {
    label: PropTypes.string,
    // defaultChecked: PropTypes.bool,
    disabled: PropTypes.bool,
    checked: PropTypes.bool,
    onChange: PropTypes.func,
  };

  constructor() {
    super();
    this.onClick = this.onClick.bind(this);
  }

  onClick(...args) {
    const { onChange } = this.props;
    if (onChange) onChange(...args);
  }

  render() {
    const { label, checked, disabled, onChange, ...restProps } = this.props;
    const extend = {};

    return (
      <Button
        {...restProps}
        data-block="uniCheckbox"
        data-mods={{ checked, disabled }}
        data-bemstyles={styles}
        onClick={this.onClick}
        disabled={disabled}
        {...extend}>
        <FlexBox row data-elem="inner">
          <div data-elem="box">
            <div data-elem="boxCheck" />
          </div>
          {label && <div data-elem="label">{label}</div>}
        </FlexBox>
      </Button>
    );
  }
}
