FROM ubuntu:latest as builder
USER root

RUN apt-get update
RUN apt-get -y install curl gnupg git
RUN curl -sL https://deb.nodesource.com/setup_12.x  | bash -
RUN apt-get -y install nodejs

RUN git clone https://gitlab.com/dryymoon/dryymoon-react-boilerplate.git /tmp
RUN cd /tmp && npm install
COPY . /tmp/src-sites/project
RUN cd /tmp/src-sites/project && npm install
RUN cd /tmp && npm run build --project=project


FROM nginx
# RUN apk add --update bash && rm -rf /var/cache/apk/*
# FROM nginx:alpine

RUN rm /etc/nginx/nginx.conf

COPY nginx.conf /etc/nginx/

COPY ./docker-entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

WORKDIR /usr/share/nginx/html

COPY --from=builder /tmp/build/project/www/ .

ENTRYPOINT ["/entrypoint.sh"]

CMD ["nginx", "-g", "daemon off;"]

EXPOSE 80
