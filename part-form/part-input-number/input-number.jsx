import React, { Component } from 'react';
import PropTypes from 'prop-types';
import trim from 'lodash/trim';
import Input from '../part-input';

export default class UniFormInputNumber extends Component {
  static propTypes = {
    value: PropTypes.oneOfType([
      PropTypes.number.isRequired,
      PropTypes.string.isRequired
    ]),
    min: PropTypes.number.isRequired,
    max: PropTypes.number.isRequired,
    onChange: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    const { value } = props;

    this.state = { value: +value };

    this.onChange = this.onChange.bind(this);
    this.onBlur = this.onBlur.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    // const isPropsValueChanged = this.props.value !== nextProps.value;
    // const isStateValueDifferentWithProps = this.state.value !== nextProps.value;
    // if (isPropsValueChanged && isStateValueDifferentWithProps) {
    if (this.currValueFromProps !== +nextProps.value) {
      this.currValueFromProps = +nextProps.value;
      this.setState({ value: +nextProps.value, });
    }
  }

  onChange(target) {
    const { onChange, min, max, value: valueFromProps } = this.props;
    let { value } = target;

    value = trim(value);
    if (value === '-') return this.setState({ value });

    value = +parseNumber(value);

    this.setState({ value }, () => {
      if (!onChange) return;
      if (value > max) value = max;
      if (value < min) value = min;
      if (+valueFromProps !== value) {
        this.currValueFromProps = value;
        onChange({ ...target, value });
      }
    });
  }

  onBlur() {
    const { value: valueInProps } = this.props;
    const { value: valueInState } = this.state;
    if (valueInState !== +valueInProps) {
      this.setState({ value: +valueInProps });
    }
  }

  render() {
    const { value } = this.state;

    return (
      <Input
        {...this.props}
        {...{
          value,
          onChange: this.onChange,
          onBlur: this.onBlur
        }} />
    );
  }
}

const regexp = /([-+]?[0-9]*\.?[0-9]+)/;
function parseNumber(string = '') {
  const match = string.match(regexp);
  if (match) return match[0];
  return NaN;
}

/* function isInt(n) {
 return n % 1 === 0;
 }

 function isFloat(n) {
 return n % 1 !== 0;
 } */
